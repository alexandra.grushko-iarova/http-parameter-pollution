from flask import Flask, request

app = Flask(__name__)

@app.route('/api/payment', methods=['GET', 'POST'])
def main():
    action = request.form.get('action')
    amount = request.form.get('amount')
    print(request.form.getlist('action'))
    print(action)
    print(amount)
    if amount and action:
        if action == 'withdraw':
            return 'Successfully withdrew ${}'.format(amount)
        if action == 'transfer':
            return 'Successfully transfered ${}'.format(amount)
    return 'You must specify action and amount'
