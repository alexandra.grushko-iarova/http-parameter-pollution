using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using program.Models;

namespace program.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActionItemsController : ControllerBase
    {
        private readonly ActionItemContext _context;
        private readonly string _paymentUrl = Environment.GetEnvironmentVariable("paymenturl");
        private string[] _allowedValues = new[] {nameof(ActionItem.Action).ToLower(), nameof(ActionItem.Amount).ToLower()};

        public ActionItemsController(ActionItemContext context)
        {
            _context = context;
        }

        // GET: api/ActionItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActionItem>>> GetActionItem()
        {
            return await _context.ActionItem.ToListAsync();
        }

        // GET: api/ActionItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActionItem>> GetActionItem(long id)
        {
            var actionItem = await _context.ActionItem.FindAsync(id);

            if (actionItem == null)
            {
                return NotFound();
            }

            return actionItem;
        }

        // PUT: api/ActionItems/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActionItem(long id, ActionItem actionItem)
        {
            if (id != actionItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(actionItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActionItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<Tuple<ActionItem, string>> PostActionItem([FromForm] ActionItem actionItem)
        {
            //Only transfer is allowed
            if (actionItem.Action != "transfer")
            {
                return Tuple.Create(new ActionItem(), "You can only transfer an amount");
            }

            if (actionItem.Amount <= int.MinValue || actionItem.Amount >= int.MaxValue)
            {
                throw new OverflowException();
            }

            if (actionItem.Amount < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
 
            var formContent = new List<KeyValuePair<string, string>>();

            foreach (var formKey in Request.Form.Keys.Where(_ => _allowedValues.Contains(_)))
            {
                foreach (var content in Request.Form[formKey].Reverse())
                {
                    formContent.RemoveAll(x => x.Key == formKey);
                    formContent.Add(new KeyValuePair<string, string>(formKey, content));
                }
            }

            var formUrlEncodedContent = new FormUrlEncodedContent(formContent);

            var httpClient = new HttpClient();

            HttpResponseMessage result = await httpClient.PostAsync(_paymentUrl, formUrlEncodedContent);
            var response = await result.Content.ReadAsStringAsync();
            //Print response from Payment service
            Console.WriteLine(response);

            _context.ActionItem.Add(actionItem);
            await _context.SaveChangesAsync();

            //return CreatedAtAction(nameof(GetActionItem), new { id = actionItem.Id }, actionItem);
            return Tuple.Create(actionItem, response);

        }


        // DELETE: api/ActionItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ActionItem>> DeleteActionItem(long id)
        {
            var actionItem = await _context.ActionItem.FindAsync(id);
            if (actionItem == null)
            {
                return NotFound();
            }

            _context.ActionItem.Remove(actionItem);
            await _context.SaveChangesAsync();

            return actionItem;
        }

        private bool ActionItemExists(long id)
        {
            return _context.ActionItem.Any(e => e.Id == id);
        }
    }
}
