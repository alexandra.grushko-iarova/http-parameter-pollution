using Microsoft.EntityFrameworkCore;

namespace program.Models
{
    public class ActionItemContext : DbContext
    {
        public ActionItemContext(DbContextOptions<ActionItemContext> options)
            : base(options)
        {}

        public DbSet<ActionItem> ActionItem { get; set; }
    }
}
