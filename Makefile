APP=secdim.lab.csharp

all: build

test:
	docker-compose up -d payment
	docker build --tag=$(APP).verify --network $(APP).hpp --build-arg TESTSPEC=usability --build-arg paymenturl=http://payment:5000/api/payment --rm .
	docker-compose down

securitytest:
	docker-compose up -d payment
	docker build --tag=$(APP).verify --network $(APP).hpp --build-arg TESTSPEC=security --build-arg paymenturl=http://payment:5000/api/payment --rm .
	docker-compose down

build:
	docker-compose build

run:
	docker-compose up
	#docker-compose run --rm --service-ports verify dotnet program.dll
	#docker-compose down

clean:
	docker-compose down
	docker-compose rm -f
	docker system prune
	docker image prune -f

.PHONY: all test securitytest clean
